package ua.ithillel.dnipro.kremena.homework.util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

    public static List<String> scanDirectory(String path) {
        List<String> listFiles = new ArrayList<>();
        for (File file : new File(path).listFiles()) {
            if (file.getName().equals(".") || file.getName().equals("..")) {
                continue;
            }
            if (file.isDirectory()) {
                listFiles.addAll(scanDirectory(file.getPath()));
            } else {
                listFiles.add(file.getAbsolutePath());
            }
        }
        return listFiles;
    }

    public static void fileWrite(List<String> list, String nameFile) {
        File file = new File(nameFile);
        createDirectoryAndFile(file);
        try(OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            for (String s : list) {
                outputStream.write(s.concat("\n").getBytes(StandardCharsets.UTF_8));
            }
            outputStream.flush();
        } catch (IOException iOE) {
            System.out.println("Ошибка записи в файл : " + iOE.getMessage());
        }
    }

    private static void createDirectoryAndFile(File file) {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException iOE) {
                System.out.println("Ошибка создания файла : " + iOE.getMessage());
            }
        }
    }

}