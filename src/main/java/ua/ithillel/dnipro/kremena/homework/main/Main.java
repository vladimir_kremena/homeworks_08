package ua.ithillel.dnipro.kremena.homework.main;

import ua.ithillel.dnipro.kremena.homework.util.FileUtil;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String> listFiles = FileUtil.scanDirectory(".");

        FileUtil.fileWrite(listFiles, "./out_directory/out_file.txt");

    }
}